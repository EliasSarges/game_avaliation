Jogo de tiro, plataforma 2d, multiplayer local, com 2 jogadores simultaneos, o 
objetivo do jogo é sobreviver ao jogador inimigo, quem conseguir diminuir a 
vida do jogador adversario a 0 zero, vence o jogo.

**OBS quando um jogador vence, a vitoria aparece no console, junto com a mensagem
perguntando se deseja continuar a partida. Para iniciar o jogo, é preciso rodar
o script Main**


**Controles do jogador 1** - jogador da esquerda.

**A** - ESQUERDA 
**D** - DIREITA
**W** - PULO
**G** - TIRO

**Controles do jogador 2** - jogador da direita.

**DIRECIONAL ESQUERDO** - ESQUERDA

**DIRECIONAL DIREITO** - DIREITA

**DIRECIONAL PARA CIMA** - PULO

**CTRL DIREITO** - TIRO
