import pygame, sys
#player1 idle
idle1 = [pygame.image.load("sprites\\player1\\idle\\1.png"), 
        pygame.image.load("sprites\\player1\\idle\\2.png"), 
        pygame.image.load("sprites\\player1\\idle\\3.png"), 
        pygame.image.load("sprites\\player1\\idle\\4.png")]

#player1 idle
walk1 = [pygame.image.load("sprites\\player1\\walk\\1.png"), 
        pygame.image.load("sprites\\player1\\walk\\2.png"), 
        pygame.image.load("sprites\\player1\\walk\\3.png"), 
        pygame.image.load("sprites\\player1\\walk\\4.png")]

#idle animation second variation
idle2 = [pygame.image.load("sprites\\player2\\idle\\1.png"), 
        pygame.image.load("sprites\\player2\\idle\\2.png"), 
        pygame.image.load("sprites\\player2\\idle\\3.png"), 
        pygame.image.load("sprites\\player2\\idle\\4.png")]

#walk animation second variation
walk2 = [pygame.image.load("sprites\\player2\\walk\\1.png"), 
        pygame.image.load("sprites\\player2\\walk\\2.png"), 
        pygame.image.load("sprites\\player2\\walk\\3.png"), 
        pygame.image.load("sprites\\player2\\walk\\4.png")]

