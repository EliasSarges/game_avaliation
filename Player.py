from Settings import *
from Sprites import *


#classe da plataforma
class Plataform():
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

#objetos plataformas
ptl_ground1 = Plataform(255 ,244, 180, 1)#posição x, posição y, largura e altura
ptl_ground2 = Plataform(85 ,322, 70, 1)#posição x, posição y, largura e altura
ptl_ground3 = Plataform(560 ,322, 65, 1)#posição x, posição y, largura e altura


#classe do player
class Player():
    def __init__(self, direction,  key_left, key_right, key_up, idle, walk, height, width, pos):
        self.life = 3 # vida do player
       
        #atributos de movimento 
        self.left = False
        self.right = False
        self.direction = direction
        self.pos = pos
        
        self.shooting = False #controle do tiro do player

        #atributos de teclas
        self.key_left = key_left
        self.key_right = key_right
        self.key_up = key_up
        self.height = height
        self.width = width

        #atributos de animações
        self.idle = idle
        self.walk = walk
        self.IMAGE_INTERVAL = 180  
        self.last_update = 0  
        self.current_image = 0  

        #atributos de gravidade e pulo 
        self.jump = 10
        self.on_ground = True
        self.velocity = 0
        self.gravity = 0.50

        self.image = pygame.image.load("sprites\\player1\\idle\\1.png") #recebe uma imagem qualquer pra transformar o atributo em surface

    
    
        

    #atualiza o player, verifica gravidade, gerencia as animações e as colisões
    def update(self): 
        
        #controla a animação do player, muda as animações e controla o tempo das animações 
        if pygame.time.get_ticks() - self.last_update > self.IMAGE_INTERVAL: #a função get_ticks obtem o tempo em milissegundos
            self.current_image += 1 #indice da lista da animação 
            self.current_image = self.current_image % 4 
            self.last_update = pygame.time.get_ticks()
        
        screen.blit(self.image, [self.pos[0], self.pos[1]]) #desenha o player


        '''ao soltar a tecla de movimento, verifica a direção final do jogador
           inverte a imagem de inatividade e adiciona no atributo de imagem a que sera desenhada na tela'''
        if not self.right:
            if self.direction == 0:
                self.image = pygame.transform.flip(self.idle[self.current_image], True, False)
            if self.direction == 1:
                self.image = self.idle[self.current_image]
        
        #animação de caminhada
        if self.right:
            self.image = self.walk[self.current_image]
        if self.left:
            self.image = pygame.transform.flip(self.walk[self.current_image], True, False)
        

        #atualização da gravidade
        self.velocity += self.gravity
        self.pos[1] += self.velocity
        
        #atualiza a posição do jogador
        if self.right:
            self.pos[0] += 4
        if self.left:
            self.pos[0] -= 4

        #verifica a colisão com o chão
        if self.pos[1] + 128 > WINDOW_SIZE[1]:
            self.pos[1] = WINDOW_SIZE[1] - 128
            self.velocity = 0
            self.on_ground = True
        
        #verifica a colisão com as plataforma 1
        if (self.pos[0] <= ptl_ground1.x + ptl_ground1.width and self.pos[0] + self.width >= ptl_ground1.x and
            self.pos[1] <= ptl_ground1.y + ptl_ground1.height and self.pos[1] + self.height >= ptl_ground1.y):
            self.on_ground = True
            self.velocity = 0
            self.pos[1] = ptl_ground1.y - self.height

        #verifica a colisão com as plataforma 2
        if (self.pos[0] <= ptl_ground2.x + ptl_ground2.width and self.pos[0] + self.width >= ptl_ground2.x and
            self.pos[1] <= ptl_ground2.y + ptl_ground2.height and self.pos[1] + self.height >= ptl_ground2.y):
            self.on_ground = True
            self.velocity = 0
            self.pos[1] = ptl_ground2.y - self.height

        #verifica a colisão com as plataforma 3
        if (self.pos[0] <= ptl_ground3.x + ptl_ground3.width and self.pos[0] + self.width >= ptl_ground3.x and
            self.pos[1] <= ptl_ground3.y + ptl_ground3.height and self.pos[1] + self.height >= ptl_ground3.y):
            self.on_ground = True
            self.velocity = 0
            self.pos[1] = ptl_ground3.y - self.height

        #verifica a  colisão com a tela 
        if self.pos[0] <= 0:
            self.pos[0] = 0
        if self.pos[0] >= WINDOW_SIZE[0] - self.width:
            self.pos[0] = WINDOW_SIZE[0] - self.width
    
    







    
