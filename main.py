from Player import *

playing = True #controla a finalização do jogo
pygame.init() #inicia o modulo  pygame
pygame.font.init() #inicia o modulo de fonte

score1 = 0 # placar do player 1
score2 = 0 # placar do player 2


def game():
    #objetos da classe player
    player1 = Player(1, 97, 100, 119, idle1, walk1, 60, 40, [100,200])#direção, teclas, animações, altura, largura e posição
    player2 = Player(0, 276, 275, 273, idle2, walk2, 60, 75, [560,200])#direção, teclas, animações, altura, largura e posição


    #carrega a imagem do background
    bg = pygame.image.load("sprites\\scene\\background.jpg")


    #classe da bala
    class Bullet():
        def __init__(self, which): # o atributo which seleciona qual instancia da casse player tera a posição copiada
            self.image = pygame.image.load("sprites\\bullet\\bullet.png") #carrega a imagem da bala
            self.pos = [which.pos[0], which.pos[1]] 
            self.w = 16
            self.h = 10

            
            
        def update(self, which): #atualiza localização da bala em relação ao player
            self.pos[0] = which.pos[0] + 40
            self.pos[1] = which.pos[1] + 35
        
        
        #desenha a bala na tela e muda a direção de acordo com a direção do player
        def draw(self, which): 
            if which.direction == 1:
                self.pos[0] += 30
                screen.blit(self.image, [self.pos[0], self.pos[1]])
            
            if which.direction == 0:
                self.pos[0] -= 30
                screen.blit(self.image, [self.pos[0], self.pos[1]])
        
    #instancias das balas
    bullet1 = Bullet(player1) 
    bullet2 = Bullet(player2)


   
   

    GAME_FONT = pygame.font.Font("font\\pixelart.ttf", 25) #armazena o tipo de fonte e o tamanho da fonte


    win_text1 = GAME_FONT.render("Bananada Venceu! ", False, (0, 0, 0)) #texto suavização e cor, tela de fim de jogo
    win_text2 = GAME_FONT.render("Coco Pistoleiro Venceu! ", False, (0, 0, 0)) #texto suavização e cor, tela de fim de jogo

    #game loop  
    while True: 
        screen.blit(bg, (0,0))#desenha o background
        
        #chama os metodos de atualização dos players
        player1.update()
        player2.update()
        
        #torna possivel o acesso das variaveis fora da função
        global score1
        global score2

        #atualiza os dados da inteface
        life_text1 = GAME_FONT.render("Life  " + str(player1.life), False, (0, 0, 0)) #texto, suavidade e cor
        life_text2 = GAME_FONT.render("Life  " + str(player2.life), False, (0, 0, 0)) #texto, suavidade e cor
        score_text1 = GAME_FONT.render("Score  " + str(score1), False, (0, 0, 0)) #texto, suavidade e cor
        score_text2 = GAME_FONT.render("Score  " + str(score2), False, (0, 0, 0)) #texto, suavidade e cor
        
        
        
        #desenha na tela os dados da interface
        screen.blit(life_text1, (20, 10)) #vida do jogador 1
        screen.blit(life_text2, (540, 10)) #vida do jogador 2
        screen.blit(score_text1, (540, 40)) #placar do jogador 1
        screen.blit(score_text2, (20, 40)) #placar do jogador 2

        #mostra quem venceu e atualiza o score do player vencedor
        if player1.life == 0:
            score1 += 1
            break
        if player2.life == 0:
            score2 += 1
            break
        
        #detecta a colisão da bala com a janela    
        if bullet1.pos[0] >= WINDOW_SIZE[0]:
            bullet1.update(player1)
            player1.shooting = False
        if bullet1.pos[0] <= 0:
            bullet1.update(player1)
            player1.shooting = False

        #detecta a colisão da bala com a janela
        if bullet2.pos[0] >= WINDOW_SIZE[0]:
            bullet2.update(player2)
            player2.shooting = False
        if bullet2.pos[0] <= 0:
            bullet2.update(player2)
            player2.shooting = False

        #controle da chamada do tiro do player 1
        if player1.shooting:
            bullet1.draw(player1)
        else:
            bullet1.update(player1)
        
        #controle da chamada do tiro do player 2    
        if player2.shooting:
            bullet2.draw(player2)
        else:
            bullet2.update(player2)

        
        #detecta a colisão da bala com o player inimigo 
        if (bullet1.pos[0] <= player2.pos[0] + player2.width and bullet1.pos[0] + bullet1.w >= player2.pos[0] and
            bullet1.pos[1] <= player2.pos[1] + player2.height and bullet1.pos[1] + bullet1.h >= player2.pos[1]):
            if player1.shooting:
                player2.life -= 1
                player1.shooting = False
                player1.update()

        #detecta a colisão da bala com o player inimigo
        if (bullet2.pos[0] <= player1.pos[0] + player1.width and bullet2.pos[0] + bullet2.w >= player1.pos[0] and
            bullet2.pos[1] <= player1.pos[1] + player1.height and bullet2.pos[1] + bullet2.h >= player1.pos[1]):
            if player2.shooting:
                player1.life -= 1
                player2.shooting = False
                player2.update()
    

        #captura de eventos
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

            #ao apertar a tecla 
            if event.type == KEYDOWN:  
                
                #pulo do player 1
                if event.key == player1.key_up and player1.on_ground == True: 
                    player1.velocity = -player1.jump
                    player1.on_ground = False

                #tiro do player 1
                if event.key == 103:
                    player1.shooting = True

                #movimento do player 1
                if event.key == player1.key_left:
                    player1.left = True
                    if player1.shooting == False: #so muda a direção quando o player não ta atirando
                        player1.direction = 0

                if event.key == player1.key_right:  
                    player1.right = True 
                    if player1.shooting == False: #so muda a direção quando o player não ta atirando
                        player1.direction = 1
                
                #tiro do player 1
                if event.key == 305:
                    player2.shooting = True
                
                #pulo do player 2
                if event.key == player2.key_up and player2.on_ground == True: 
                    player2.velocity = -player2.jump
                    player2.on_ground = False

                #movimento do player 2
                if event.key == player2.key_left:  
                    player2.left = True
                    if player2.shooting == False: #so muda a direção quando o player não ta atirando
                        player2.direction = 0

                if event.key == player2.key_right: 
                    player2.right = True 
                    if player2.shooting == False: #so muda a direção quando o player não ta atirando
                        player2.direction = 1


            #ao soltar a tecla
            if event.type == KEYUP:
                #player 1
                if event.key == player1.key_left:
                    player1.left = False
                if event.key == player1.key_right:
                    player1.right = False     
                    
                #player 2
                if event.key == player2.key_left:
                    player2.left = False
                if event.key == player2.key_right:
                    player2.right = False     
        
        pygame.display.update() #atualiza a tela
        clock.tick(60) #mantem os fps em 60

game()

while playing:
    
    answer = input("Deseja jogar novamente? [Y/N]: ")
    if answer == "y":
        game()
    else:
        break


    
        
